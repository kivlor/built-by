---
---

# wait for the page to load
window.onload = () -> 
  # find and loop any models
  elems = document.querySelectorAll '.model'
  Array.prototype.forEach.call elems, (el, i) ->
    # grab the file and title
    file = el.getAttribute 'data-file'
    title = el.getAttribute 'data-title'

    # create an iframe
    frame = document.createElement 'iframe'
    frame.frameBorder = 0
    frame.width = el.offsetWidth
    frame.height = Math.floor(content.offsetWidth * 0.5)
    frame.src = "https://render.githubusercontent.com/view/3d?url=https://raw.githubusercontent.com/#{ file }"

    # add to elem
    el.appendChild frame

    # remove the loading class
    if el.classList then el.classList.remove 'loading'